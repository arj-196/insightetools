"""
# Schedule Extraction:

    - Check if new dataset is uploaded
        - if exist
            - check if try attribute less than 3
                - if less
                    - Upload file to Object Storage using the Swift Client
                    - Schedule Extraction task
                - if more
                    - add task to error list
"""

from pymongo import MongoClient
import os
import subprocess
from datetime import datetime
import keystoneclient.v3 as keystoneclient
import swiftclient.client as swiftclient
import sys, traceback
import json

# environment variables
mongo_url = "mongodb://151.80.41.13:27018"

_root_dir = os.getcwd()
root_scripts = _root_dir + '/../shell_scripts/'
root_downloaded_files = '/home/insighte/processing/uniperformer/insightetools/tmp/uploads/'
root_completed_files = _root_dir + '/../tmp_done/'

script_upload_prefix = 'upload_to_os_'
script_download_prefix = 'download_from_os_'

dir_output = 'swift://output.spark/'
# dir_output = '/Users/arj/app/bluemix_snippets/pyspark/output/'

# object storage credentials
auth_url = 'https://identity.open.softlayer.com' + '/v3'
password = 'RVz4/Ftgb!z1~U,?'
project_id = '6c9da49a81da4859bf366da4d94603c5'
user_id = '2a13810f3d164885a7c33e5894bddd6b'
region_name = 'dallas'

container_download = 'notebooks'
container_output = 'output'

swift_configurations = """
# exporting swift configurations
export OS_USER_ID=2a13810f3d164885a7c33e5894bddd6b
export OS_PASSWORD=RVz4/Ftgb!z1~U,?
export OS_PROJECT_ID=6c9da49a81da4859bf366da4d94603c5
export OS_AUTH_URL=https://identity.open.softlayer.com/v3
export OS_REGION_NAME=dallas
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_VERSION=3
"""

cmd_swift_upload = swift_configurations + """

cd $DIRNAME$
/usr/local/bin/swift upload notebooks $FILENAME$
"""
LOGDB = MongoClient(mongo_url).schedule.log

os.environ['PATH_RESOURCES'] = 'INENV'
os.environ['VAR_USER'] = 'hello@thedatastrategy.com'
os.environ['VAR_PWD'] = 'pdstds99'
os.environ['VAR_SMTPHOST'] = 'smtp.1and1.pl'
os.environ['VAR_INSIGHTADDR'] = 'insightE@thedatastrategy.com'
os.environ['VAR_REPORTTOADDRS'] = 'arjruler93@gmail.com'
from unitools2.mailer import mailer
from unitools2.mailer import resources as mrs

os.environ['VAR_HOST'] = "mongodb://151.80.41.13:27018"
os.environ['VAR_SUPPORTAPI_HOST'] = "151.80.41.13:5000"
from unitext3.insight import resources as rs


def schedule_extraction():
    db = MongoClient(mongo_url).schedule
    job = db.extractions1.find_one()

    if job:
        if 'try' not in job or job['try'] < 3:
            db.extractions1.update({'_id': job['_id']}, {'$set': {'active': True}})  # set state to active

            script_upload = script_upload_prefix + job['_id']
            # write shell script to upload dataset to object storage
            if os.path.exists(root_scripts + script_upload):
                os.remove(root_scripts + script_upload)

            f = open(root_scripts + script_upload, 'w')
            cmd = cmd_swift_upload
            cmd = cmd.replace('$DIRNAME$', root_downloaded_files)
            cmd = cmd.replace('$FILENAME$', job['filename'])
            f.write(cmd)
            f.flush()
            f.close()

            # run script upload file
            shellCmd = ['sh', root_scripts + script_upload]
            subprocess.call(shellCmd)
            # delete script upload file
            rmFileCmd = ['rm', root_scripts + script_upload]
            subprocess.call(rmFileCmd)

            # creating swift client connection
            conn = swiftclient.Connection(
                key=password,
                authurl=auth_url,
                auth_version='3',
                os_options={"project_id": project_id,
                            "user_id": user_id,
                            "region_name": region_name})


            try:
                conn.get_object(container_download, job['filename'])
                # if file exists
                # set request to queue
                job['active'] = False
                db.queue.insert(job)
                db.extractions1.remove({'_id': job['_id']})
            except:
                # file not found
                pass
        else:
            # scheduling has been tried 3 times, and it did not work, so put in error list
            db.errors.insert(job)
            db.extractions1.remove({'_id': job['_id']})

            Mailer = mailer.Mailer()
            Mailer.send(
                mrs.INSIGHTADDR,
                mrs.INSIGHTADDR,
                'Pushed to Error at Bluemix 1_schedule_extraction',
                "Pushed to ERROR!" + " \n\n ---- \n " + json.dumps(job),
                ifBodyDecorate=False
            )


if __name__ == '__main__':
    schedule_extraction()
