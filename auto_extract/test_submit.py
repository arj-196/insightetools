"""
# Do Extraction

    - Check if job exists in Queue
        - if job exists
            - Check if file exists
                - if exist
                    - do extraction
                    - save dataframe to output
                    - set job completed
                - if not exist
                    - increment try attribute
                    - set job to extraction (again)
"""
import os
from pymongo import MongoClient
from datetime import datetime
import pandas as pd
import sys, traceback
import keystoneclient.v3 as keystoneclient
import swiftclient.client as swiftclient
from pyspark import SparkContext
from pyspark import SparkConf
from pyspark.sql import SQLContext
import json
import time

# environment variables
conf = SparkConf()
conf.setAppName('Schedule Insight Extractor')
# conf.set("spark.executor.memory", "5g")
# conf.set("spark.driver.memory", "5g")
# conf.set("spark.worker.memory", "5g")

sc = SparkContext('local', 'Schedule Insight Extractor', conf=conf)
sqlContext = SQLContext(sc)

# object storage credentials
auth_url = 'https://identity.open.softlayer.com' + '/v3'
password = 'RVz4/Ftgb!z1~U,?'
project_id = '6c9da49a81da4859bf366da4d94603c5'
user_id = '2a13810f3d164885a7c33e5894bddd6b'
region_name = 'dallas'

# mongo_url = 'localhost'
mongo_url = "mongodb://151.80.41.13:27018"
support_api_host = "151.80.41.13:5000"

container_download = 'notebooks'
container_output = 'output'

os.environ['PATH_RESOURCES'] = 'INENV'
os.environ['VAR_USER'] = 'hello@thedatastrategy.com'
os.environ['VAR_PWD'] = 'pdstds99'
os.environ['VAR_SMTPHOST'] = 'smtp.1and1.pl'
os.environ['VAR_INSIGHTADDR'] = 'insightE@thedatastrategy.com'
os.environ['VAR_REPORTTOADDRS'] = 'arjruler93@gmail.com'
from unitools2.mailer import mailer
from unitools2.mailer import resources as mrs

os.environ['VAR_HOST'] = "mongodb://151.80.41.13:27018"
os.environ['VAR_SUPPORTAPI_HOST'] = "151.80.41.13:5000"
from unitext3.insight import resources as rs

LOGDB = MongoClient(mongo_url).schedule.log

#
# METHODS
#
JOB = None


def get_time():
    print "time", datetime.now() - startTime


def __do_extract(verbatim):
    import os
    os.environ['PATH_RESOURCES'] = 'INENV'  # To indicate to InsightExtractor to look for
    # settings in environment variables, instead of
    # loading from a resource file
    os.environ['VAR_HOST'] = mongo_url
    os.environ['VAR_SUPPORTAPI_HOST'] = support_api_host
    from unitext3.insight import insightextractor
    extractor = insightextractor.BaseInsightExtactor(lang=JOB['lang'])
    d = extractor.extract(
        verbatim, JOB['topic'],
        JOB['project'], JOB['user'], JOB['company'],
        ifsave=False,
        prefetcheddata=None,
        ifconcept=False, ifkeyword=True, ifterm=True,
        ifpos=True, ifsentiment=True, ifemotion=True,
        ifHashTags=True, ifMentions=True, ifUrl=True,
        ifNER=False,  # ner always False
    )
    return {'job': JOB, 's': True, 'verbatim': verbatim, 'data': json.dumps(d)}


def extract_insight(verbatim):
    try:
        return __do_extract(verbatim.encode('utf-8'))
    except:
        try:
            return __do_extract(verbatim)
        except:
            return {'job': JOB, 's': False, 'verbatim': verbatim,
                    'data': "EXCEPTION"}


def do_extraction():
    LOGDB.insert({'stage': 2, 's': 0, 'd': datetime.now(), 'r': sc._conf.getAll()})

    db = MongoClient(mongo_url).schedule
    job = db.queue.find_one({'active': False, 'done': False})

    if job:
        try:

            LOGDB.insert({'stage': 2, 's': 1, 'd': datetime.now(), 'r': ''})
            db.queue.update({'_id': job['_id']}, {'$set': {'active': True}})  # set state to active

            # creating swift client connection
            conn = swiftclient.Connection(
                key=password,
                authurl=auth_url,
                auth_version='3',
                os_options={"project_id": project_id,
                            "user_id": user_id,
                            "region_name": region_name})

            # make JOB global
            global JOB
            JOB = job

            LOGDB.insert({'stage': 2, 's': 1, 'd': datetime.now(), 'r': ''})

            try:
                obj = conn.get_object(container_download, job['splitfile'])
                lines = json.loads(obj[1])
                text_file = sc.parallelize(lines)
                resRDD = text_file.map(extract_insight)

                # converting RDD to json
                df = sqlContext.createDataFrame(resRDD)
                pdf = df.toPandas()
                jdf = pdf.to_json()

                iffileuploaded = False
                _try = 0
                while not iffileuploaded:
                    # saving json to object storage
                    conn.put_object(container_output, splitfile + '.json', jdf)

                    # check if result obj saved
                    try:
                        conn.get_object(container_output, splitfile + '.json')
                        iffileuploaded = True
                    except:
                        if _try < 3:
                            _try += 1
                        else:
                            iffileuploaded = True
                            db.queue.update({'_id': job['_id']}, {'$set': {'active': False}})
                            LOGDB.insert(
                                {'job': job, 's': -3, 'stage': 2, 'd': datetime.now(),
                                 'e': "\n".join(traceback.format_exc().splitlines())})

            except:
                db.queue.update({'_id': job['_id']}, {'$set': {'active': False}})
                LOGDB.insert(
                    {'job': job, 's': -4, 'stage': 2, 'd': datetime.now(),
                     'e': "\n".join(traceback.format_exc().splitlines())})


            job['active'] = False
            job['done'] = True

            # extraction completed
            db.queue.update({'_id': job['_id']}, {'$set': {'done': True, 'active': False}})
            LOGDB.insert({'stage': 2, 'job': job, 's': 4, 'd': datetime.now(), 'r': ''})

        except:
            e = sys.exc_info()[0]
            print ("ERROR %s", e)
            traceback.print_exc(file=sys.stdout)
            LOGDB.insert(
                {'job': job, 's': -1, 'stage': 2, 'd': datetime.now(),
                 'e': "\n".join(traceback.format_exc().splitlines())})

            # if error increment attr try and set to reschedule
            if 'try' not in job:
                job['try'] = 1
            else:
                job['try'] += 1

            job['active'] = False
            db.extractions1.insert(job)
            db.queue.remove({'_id': job['_id']})

            Mailer = mailer.Mailer()
            Mailer.send(
                mrs.INSIGHTADDR,
                mrs.INSIGHTADDR,
                'Error at Bluemix 2_do_extract',
                "<br>".join(traceback.format_exc().splitlines()) + "<br><br> ---- <br>" + json.dumps(job),
                ifBodyDecorate=False
            )

            LOGDB.insert({'stage': 2, 'job': job, 's': -12, 'd': datetime.now(), 'r': ''})


if __name__ == '__main__':
    do_extraction()
