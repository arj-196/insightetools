"""
# Fill DB From Pickle

    - Check if new job in downloaded
        - if exist
            - check if RDD dump exists
                - if exist
                    - load RDD dump, and fill db
                - if not
                    - put job in done to try again  # TODO here to do 3 tries ?
"""

from pymongo import MongoClient
from datetime import datetime
import os
import sys, traceback
import subprocess
import pandas
import json
import keystoneclient.v3 as keystoneclient
import swiftclient.client as swiftclient
# enviroment variables
mongo_url = "localhost:27018"

_root_dir = os.getcwd()
root_scripts = _root_dir + '/../shell_scripts/'
root_completed_files = _root_dir + '/../tmp_done/'

script_delete_prefix = 'delete_from_os_'

swift_configurations = """
# exporting swift configurations
export OS_USER_ID=2a13810f3d164885a7c33e5894bddd6b
export OS_PASSWORD=RVz4/Ftgb!z1~U,?
export OS_PROJECT_ID=6c9da49a81da4859bf366da4d94603c5
export OS_AUTH_URL=https://identity.open.softlayer.com/v3
export OS_REGION_NAME=dallas
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_VERSION=3
"""
# object storage credentials
auth_url = 'https://identity.open.softlayer.com' + '/v3'
password = 'RVz4/Ftgb!z1~U,?'
project_id = '6c9da49a81da4859bf366da4d94603c5'
user_id = '2a13810f3d164885a7c33e5894bddd6b'
region_name = 'dallas'

container_download = 'notebooks'
container_output = 'output'

cmd_swift_delete = swift_configurations + """

/usr/local/bin/swift delete notebooks $DIRNAME_INPUT$
/usr/local/bin/swift delete output $DIRNAME_OUTPUT$
"""
LOGDB = MongoClient(mongo_url).schedule.log
client = MongoClient(mongo_url)

os.environ['PATH_RESOURCES'] = 'INENV'
support_api_host = "151.80.41.13:5000"
os.environ['VAR_HOST'] = mongo_url
os.environ['VAR_SUPPORTAPI_HOST'] = support_api_host

from unitext3.insight import indexer as INDEXER
indexer = INDEXER.InsightIndexer()
wordcloudIndexer = INDEXER.WordCloudIndexer()

os.environ['PATH_RESOURCES'] = 'INENV'
os.environ['VAR_USER'] = 'hello@thedatastrategy.com'
os.environ['VAR_PWD'] = 'pdstds99'
os.environ['VAR_SMTPHOST'] = 'smtp.1and1.pl'
os.environ['VAR_INSIGHTADDR'] = 'insightE@thedatastrategy.com'
os.environ['VAR_REPORTTOADDRS'] = 'arjruler93@gmail.com'
from unitools2.mailer import mailer
from unitools2.mailer import resources as mrs

os.environ['VAR_HOST'] = "mongodb://151.80.41.13:27018"
os.environ['VAR_SUPPORTAPI_HOST'] = "151.80.41.13:5000"
from unitext3.insight import resources as rs


def _put_row_to_db(D):
    try:
        s = D[1]['s']  # status
        if s:
            j = D[1]['job']  # job details
            project = j['project']
            user = j['user']
            company = j['company']
            topic = j['topic']
            lang = j['lang']

            #
            # verbatim data
            d = json.loads(D[1]['data'])  # data
            verbatim = D[1]['verbatim']

            verbatim_normalized = d['text_normalized']
            terms = d['terms']
            keywords = d['keywords']
            sentiment = d['sentiment']
            hashtags = d['hashtags']
            pos = d['pos']
            emotions = d['emotions']
            entities = d['entities']
            urls = d['urls']
            concepts = d['concepts']
            mentions = d['mentions']
            prefected_data = d['prefected_data']

            indexer.index(
                verbatim_normalized,
                verbatim,
                topic,
                project,
                user,
                company,
                keywords,
                concepts,
                pos,
                terms,
                sentiment,
                hashtags,
                mentions,
                urls,
                emotions,
                entities,
                lang,
                prefetcheddata=prefected_data
            )
    except:
        pass


def put_dump_to_db(job, dump_file):
    df = pandas.read_json(dump_file)

    # iterate throw all rows
    for D in df.iterrows():
        _put_row_to_db(D)

    # extract ner entities
    execute_ner_extractor(job)

    # do the wordcloud required indexation
    wordcloudIndexer.index(job['topic'])


def execute_ner_extractor(job):
    topic = job['topic']
    global_organizations = [d['e'].encode('utf8').strip().lower() for d in
                            client.cache.entities.find({'t': 'ORGANIZATION'}, no_cursor_timeout=True)]
    global_locations = [d['e'].encode('utf8').strip().lower() for d in
                        client.cache.entities.find({'t': 'LOCATION'}, no_cursor_timeout=True)]

    lang = client.indexes.settings.find_one({"topic": topic})['lang']
    stop_words = rs.stop_words[lang]
    organizations = list(set(global_organizations) - set(stop_words))
    locations = list(set(global_locations) - set(stop_words))

    for s in client.indexes.verbatims.find({"topic": topic}, {"_id": 1, 'verbatim': 1}, no_cursor_timeout=True):
        try:
            ws = [w.encode('utf8').strip().lower() for w in words(s['verbatim'])]
            cs = count(ws)
            bad_words = list(set(ws) & set(stop_words))
            good_words = list(set(ws) - set(bad_words))

            # extract entities
            found_organizations = list(set(good_words) & set(organizations))
            found_locations = list(set(good_words) & set(locations))

            # format entities
            orgs = {}
            for org in found_organizations:
                _org = org.strip().lower()
                if _org in cs:
                    orgs[org] = cs[_org]

            locs = {}
            for org in found_locations:
                _org = org.strip().lower()
                if _org in cs:
                    locs[org] = cs[_org]

            if orgs or locs:
                entities = {
                    "LOCATION": locs,
                    "ORGANIZATION": orgs
                }

                # saving
                client.indexes.verbatimners.update(
                    {'_id': s['_id']},
                    {
                        'topic': topic,
                        'entities': entities
                    },
                    upsert=True
                )
        except:
            e = sys.exc_info()[0]
            print ("ERROR %s", e)
            traceback.print_exc(file=sys.stdout)

    # finally a little cleaning
    client.indexes.verbatimners.remove({"entities.ORGANIZATION": {}, "entities.LOCATION": {}})


def email_success(job):
    user = client.client.users.find_one({'_id': job['user']})
    if user and 'email' in user:
        Mailer = mailer.Mailer()
        Mailer.send(
            mrs.INSIGHTADDR,
            user['email'],
            'Dataset Uploaded',
            'Dataset ' + job['topic'] + ' is uploaded!'
        )


def fill_db_from_dump():
    db = client.schedule
    job = db.downloaded.find_one({'active': False})

    if job:
        if db.downloaded.count({'active': True}) < 5:

            print 'scheduling fill db'
            dump_file = root_completed_files + job['_id'] + '.json'
            try:
                db.downloaded.update({'_id': job['_id']}, {'$set': {'active': True}})  # set state to active

                if not os.path.exists(dump_file):
                    raise Exception("Dump not found")

                # putting dump to DB
                put_dump_to_db(job, dump_file)

                email_success(job)

                # finally if all good, delete dump
                print 'all completed, deleting dump'
                # delete dump from local
                os.remove(dump_file)

                # write shell script to delete dump from object storage
                script_delete = script_delete_prefix + job['_id'] + '.json'
                if os.path.exists(root_scripts + script_delete):
                    os.remove(root_scripts + script_delete)

                f = open(root_scripts + script_delete, 'w')
                cmd = cmd_swift_delete
                cmd = cmd.replace('$DIRNAME_INPUT$', job['filename'])
                cmd = cmd.replace('$DIRNAME_OUTPUT$', job['_id'] + '.json')
                f.write(cmd)
                f.flush()
                f.close()

                # run script upload file
                shellCmd = ['sh', root_scripts + script_delete]
                subprocess.call(shellCmd)
                # delete script upload file
                rmFileCmd = ['rm', root_scripts + script_delete]
                subprocess.call(rmFileCmd)

                # creating swift client connection
                conn = swiftclient.Connection(
                    key=password,
                    authurl=auth_url,
                    auth_version='3',
                    os_options={"project_id": project_id,
                                "user_id": user_id,
                                "region_name": region_name})

                try:
                    # if file still exists, try download again
                    conn.get_object(container_output, job['_id'] + '.json')
                    db.downloaded.update({'_id': job['_id']}, {'$set': {'active': False}})  # set state to inactive
                except:
                    # file not found
                    db.downloaded.remove({'_id': job['_id']})

            except:
                e = sys.exc_info()[0]
                print ("ERROR %s", e)
                traceback.print_exc(file=sys.stdout)

                LOGDB.insert(
                    {'job': job, 'stage': 4, 's': -1, 'd': datetime.now(), 'e': "\n".join(traceback.format_exc().splitlines())})
                # dump was not downloaded
                job['active'] = False
                db.done.insert(job)
                db.downloaded.remove({'_id': job['_id']})

                Mailer = mailer.Mailer()
                Mailer.send(
                    mrs.INSIGHTADDR,
                    mrs.INSIGHTADDR,
                    'Error at Bluemix 4_fill_db_from_dump',
                    "\n".join(traceback.format_exc().splitlines()) + "\n\n ---- \n\n" + json.dumps(job),
                    ifBodyDecorate=False
                )


if __name__ == '__main__':
    fill_db_from_dump()
