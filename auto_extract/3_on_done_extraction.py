"""
# On Done Extraction:

    - Check if an extraction task is completed
        - if exist
            - Download the RDD dump from Object Storage using Swift Client
            - Store data to DB
"""

from pymongo import MongoClient
import os
import subprocess
from datetime import datetime

# environment variables
mongo_url = 'localhost:27018'

_root_dir = os.getcwd()
root_scripts = _root_dir + '/../shell_scripts/'
root_completed_files = _root_dir + '/../tmp_done/'

script_upload_prefix = 'upload_to_os_'
script_download_prefix = 'download_from_os_'

dir_output = 'swift://output.spark/'
# dir_output = '/Users/arj/app/bluemix_snippets/pyspark/output/'

swift_configurations = """
# exporting swift configurations
export OS_USER_ID=2a13810f3d164885a7c33e5894bddd6b
export OS_PASSWORD=RVz4/Ftgb!z1~U,?
export OS_PROJECT_ID=6c9da49a81da4859bf366da4d94603c5
export OS_AUTH_URL=https://identity.open.softlayer.com/v3
export OS_REGION_NAME=dallas
export OS_IDENTITY_API_VERSION=3
export OS_AUTH_VERSION=3
"""

cmd_swift_upload = swift_configurations + """

cd $DIRNAME$
/usr/local/bin/swift upload notebooks $FILENAME$
"""
cmd_swift_download = swift_configurations + """
cd $ROOTDIRNAME$
/usr/local/bin/swift download output $DIRNAME$
"""
os.environ['PATH_RESOURCES'] = 'INENV'
os.environ['VAR_USER'] = 'hello@thedatastrategy.com'
os.environ['VAR_PWD'] = 'pdstds99'
os.environ['VAR_SMTPHOST'] = 'smtp.1and1.pl'
os.environ['VAR_INSIGHTADDR'] = 'insightE@thedatastrategy.com'
os.environ['VAR_REPORTTOADDRS'] = 'arjruler93@gmail.com'
from unitools2.mailer import mailer
from unitools2.mailer import resources as mrs

os.environ['VAR_HOST'] = "mongodb://151.80.41.13:27018"
os.environ['VAR_SUPPORTAPI_HOST'] = "151.80.41.13:5000"
from unitext3.insight import resources as rs


def on_done_extraction():
    db = MongoClient(mongo_url).schedule
    job = db.done.find_one({'active': False})

    if job:
        print 'scheduling download'
        db.done.update({'_id': job['_id']}, {'$set': {'active': True}})  # set state to active

        script_download = script_download_prefix + job['_id']
        if os.path.exists(root_scripts + script_download):
            os.remove(root_scripts + script_download)

        f = open(root_scripts + script_download, 'w')
        cmd = cmd_swift_download
        cmd = cmd.replace('$ROOTDIRNAME$', root_completed_files)
        cmd = cmd.replace('$DIRNAME$', job['_id'] + '.json')
        f.write(cmd)
        f.flush()
        f.close()

        # run script download file
        shellCmd = ['sh', root_scripts + script_download]
        subprocess.call(shellCmd)
        # delete script download file
        rmFileCmd = ['rm', root_scripts + script_download]
        subprocess.call(rmFileCmd)

        # check if file exists
        if os.path.exists(root_completed_files + job['_id'] + '.json'):
            job['active'] = False
            db.downloaded.insert(job)
            db.done.remove({'_id': job['_id']})
        else:
            db.done.update({'_id': job['_id']}, {'$set': {'active': False}})  # set state to inactive

if __name__ == '__main__':
    on_done_extraction()
