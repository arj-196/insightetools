import os, json, time
from pymongo import MongoClient
from __init__ import get_task
import swiftclient.client as swiftclient
import sys, traceback
from datetime import datetime


class Automator(object):
    tasks = []
    swift_client = None

    def __init__(self, conf):
        print '--------------------'
        print '--- SUBMIT_TASK Configuration'
        print '-- db:', conf['db']['host']

        self.conf = conf
        self._setup_dir()
        self.client = MongoClient(conf['db']['host'])

        # setup tasks
        for task_conf in conf['registered_tasks']:
            options = task_conf['options']
            options['tmp_dir'] = self.conf['tmp_dir']
            options['swiftclient'] = self.conf['swiftclient']
            self.tasks.append(get_task(self.client, task_conf['type'], options))

    def _setup_swift_client(self):
        if not self.swift_client:
            # setup swift client
            obj_conf = self.conf['object_storage']
            self.swift_client = swiftclient.Connection(
                key=obj_conf['password'],
                authurl=obj_conf['auth_url'],
                auth_version='3',
                os_options={
                    "project_id": obj_conf['project_id'],
                    "user_id": obj_conf['user_id'],
                    "region_name": obj_conf['region_name']
                }
            )

    def _setup_dir(self):
        if not os.path.exists(self.conf['tmp_dir']):
            os.mkdir(self.conf['tmp_dir'])

    def automate(self, mode):
        if mode == "pre":
            self.do_task_preliminaries()
        elif mode == "post":
            self.do_task_postliminaries()

    def do_task_preliminaries(self):
        for task in self.tasks:
            r = task.do_preliminaries()
            if r['status']:
                if r['todo'] == 'upload':
                    # upload file to object storage
                    status = self.upload_to_object_storage(r['filename'], r['content'])
                    if status:
                        # upload task object
                        self.client.bm.tasks.insert(dict(
                            type=r['type'],
                            filename=r['filename'],
                            state='inactive',
                            created_at=datetime.now(),
                        ))

                        # only all procedure are finished, set task to completed
                        task.set_as_completed()
                        print '- CREATED JOB: Type', r['type'], 'filename:', r['filename']

    def upload_to_object_storage(self, filename, content):
        self._setup_swift_client()
        if_uploaded = False
        _try = 0
        status = False
        print '- UPLOADING FILE: ', filename
        while not if_uploaded:
            _try += 1
            self.swift_client.put_object(
                self.conf['object_storage']['container_upload'],
                filename,
                json.dumps(content),
                # content,
                # content_type='text/plain'
            )
            try:
                time.sleep(2)
                self.swift_client.get_object(self.conf['object_storage']['container_upload'], filename)
                if_uploaded = True
                status = True
                print '- SUCCESSFULLY UPLOADED:', filename
            except:
                pass
            # error fallback
            if _try > 4:
                print '-- ERR:', 'Could not upload file, max tries exceeded:', filename
                if_uploaded = True
        return status

    def do_task_postliminaries(self):
        for task in self.tasks:
            r = task.do_postliminaries()
            if r and r['state']:
                print '-----------------------'
                print '-- TASK COMPLETED SUCCESSFULLY'
                print '-----------------------'
                job = r['job']
                _id = job['_id']
                job['__id'] = _id
                del job['_id']
                try:
                    # insert to tasks completed
                    self.client.bm.tasks_completed.insert(job)
                    # remove from tasks queue
                    self.client.bm.tasks.delete_one({'_id': _id})
                except:
                    e = sys.exc_info()[0]
                    print ("ERROR %s", e)
                    traceback.print_exc(file=sys.stdout)
                # remove job from collection tasks
                pass
