import nertask


def get_task(dbclient, task_name, conf):
    if task_name == "ner":
        return nertask.NerTask(dbclient=dbclient, conf=conf)
