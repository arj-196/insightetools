import sys, os, json

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../../unitools/')

import codecs
from task import AutomateTask
from pymongo import errors as mongo_errors
from unitools2.file import reader
from pyspark import SparkContext


class NerTask(AutomateTask):
    sc = None

    def __init__(self, dbclient, conf):
        super(NerTask, self).__init__("NERTASK", dbclient, conf)

    def _setup_spark_context(self):
        if not self.sc:
            self.sc = SparkContext(appName="AutomateTask_" + self.name)

    def get_verbatim_batch(self):
        verbatims = self._get_verbatim_batch()
        # do cleaning
        verbatims['verbatim'] = verbatims['verbatim'].map(lambda v: v.replace('\n', ''))

        return verbatims

    def do_preliminaries(self):
        print '---', self.name, 'Doing Preliminary'
        if self.if_required_preliminary():
            verbatims = self.get_verbatim_batch()

            # create dump file
            tmp_dir, filename = self.get_tmp_file_name()
            filename += '.verbatim.csv'
            verbatims.to_csv(
                tmp_dir + filename,
                encoding='utf-8',
                header=False,
                columns=['_id', 'topic', 'verbatim', 'lang', 'type']
            )

            # read content
            content = reader.FileReader(codecs.open(tmp_dir + filename, 'rb', encoding='utf-8')).read()

            # remove unnecessary file
            os.remove(tmp_dir + filename)

            return dict(
                status=True,
                todo='upload',
                filename=filename,
                # content=content.encode('ascii', 'replace'),  # TODO handle encoding here!
                content=content,
                type='ner',
            )

    def do_postliminaries(self):
        print '---', self.name, 'Doing Postliminaries'
        task = self.get_postliminary_task()
        if task:
            print '-----------------------'
            print '-- ', self.name, 'Task at Hand', '\n'
            print '-- task', task
            print '-----------------------'

            print '-----------------------'
            print '-- ', self.name, 'Downloading Results', '\n'
            print '-----------------------'
            status, tmp_dir = self.download_results(task)

            if status:
                self._setup_spark_context()
                # load results
                results = self.sc.pickleFile(tmp_dir + task['result']['files']['results'])
                print '-----------------------'
                print '-- ', self.name, 'Loading Results', '\n'
                print results.take(5)
                print '-----------------------'

                # formatting results
                formatted_results = self.format_results(results)
                print '-----------------------'
                print '-- ', self.name, 'Formatting Results', '\n'
                print formatted_results.take(5)
                print '-----------------------'

                print '-----------------------'
                print '-- ', self.name, 'Saving To Database', '\n'
                print '-----------------------'
                # put results in database
                self.save_results_to_db(formatted_results)

                # load failed results
                # TODO handle failed verbatims
                pass

            # remove tmp_dir
            self._remove_tmp_download_files(tmp_dir)

            return dict(
                state=True,
                job=task
            )
        else:
            return None

    @staticmethod
    def format_results(results):

        def normalize_entities(e):
            return e.replace('.', '')

        def format_entities(d):
            if d[0]:
                entities = {}
                for e in d[3]:
                    ne = normalize_entities(e[0])
                    if e[1] not in entities:
                        entities[e[1]] = {}
                    if e[0] not in entities[e[1]]:
                        entities[e[1]][ne] = 1
                    else:
                        entities[e[1]][ne] += 1
                return True, d[2], entities, d[4]

            return False, d[1:]

        # formatting results
        formatted_entities = results.map(format_entities)
        return formatted_entities

    def save_results_to_db(self, formatted_results):
        def save_to_db(d):
            if d[0]:
                try:
                    # if new doc
                    self.client.indexes.verbatimners.insert_one({
                        '_id': d[1],
                        'topic': d[3],
                        'entities': d[2],
                        'enabled': True
                    })
                except mongo_errors.DuplicateKeyError:
                    # if exists
                    self.client.indexes.verbatimsners.update_one(
                        {'_id': d[1]},
                        {'$set': {'entities': d[2]}}
                    )

                # update verbatim configurations
                o = self.client.indexes.verbatims.find_one({'_id': d[1]})
                if 'conf' not in o:
                    self.client.indexes.verbatims.update_one(
                        {'_id': d[1]},
                        {'$set': {'conf': {'ner': True}}}
                    )
                else:
                    self.client.indexes.verbatims.update_one(
                        {'_id': d[1]},
                        {'$set': {'conf.ner': True}}
                    )

        # getting results as an iterable list
        results = formatted_results.collect()
        # saving data to database
        for i, r in enumerate(results):
            save_to_db(r)

            if i % 50 == 0:
                print i, r
