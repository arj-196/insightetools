import pandas as pd
from datetime import datetime
import subprocess
import uuid
import os
import shutil


class AutomateTask(object):
    MAX_VERBATIMS = 500
    _conf_map = {
        'NERTASK': 'ner',
    }
    _vids = None

    def __init__(self, name, dbclient, conf):
        self.name = name
        self.conf = conf
        self.client = dbclient
        # search query
        self.search_query = {'$or': [
            {'conf': {'$exists': False}},
            {'conf.' + self._conf_map[self.name]: {'$exists': False}},
        ]}
        self.search_query_postlimiary = {
            'type': self._conf_map[self.name], 'state': 'done'
        }

    def set_as_completed(self):
        if self._vids:
            # getting the documents where conf object exists and doesn't exist
            yes_conf = []
            no_conf = []
            verbatims = self.client.indexes.verbatims.find({'_id': {'$in': self._vids}}, {'_id': 1, 'conf': 1})
            for v in verbatims:
                if 'conf' in v:
                    yes_conf.append(v['_id'])
                else:
                    no_conf.append(v['_id'])

            # finally updating status of verbatims
            if yes_conf:
                self.client.indexes.verbatims.update_many(
                    {'_id': {'$in': yes_conf}},
                    {'$set': {
                        'conf': {
                            self._conf_map[self.name]: False  # false meaning started but not yet completed
                        }
                    }}
                )
            if no_conf:
                self.client.indexes.verbatims.update_many(
                    {'_id': {'$in': no_conf}},
                    {'$set': {
                        'conf.' + self._conf_map[self.name]: False
                    }}
                )

    def if_required_preliminary(self):
        if self.client.indexes.verbatims.count(self.search_query):
            return True
        return False

    def get_postliminary_task(self):
        return self.client.bm.tasks.find_one(self.search_query_postlimiary)

    def _get_verbatim_batch(self):
        docs = [d for d in self.client.indexes.verbatims.find(
            self.search_query,
            {'verbatim': 1, 'topic': 1, 'conf': 1})[:self.MAX_VERBATIMS]]
        df = pd.DataFrame(docs)

        # make sure that verbatims are in quotes
        df['verbatim'] = df['verbatim'].map(self._verify_verbatim_in_quotes)

        # get topic languages
        unique_topics = df.topic.unique().tolist()
        topics = [d for d in self.client.indexes.settings.find({'topic': {'$in': unique_topics}})]

        # create topic language object
        topic_langs = {}
        for topic in topics:
            topic_langs[topic['topic']] = topic['lang']

        # set topic language
        df['lang'] = df['topic'].map(lambda t: topic_langs[t])
        df['type'] = 'verbatim'

        # store the verbatim ids that have been selected
        self._vids = df['_id'].tolist()

        return df

    def get_tmp_file_name(self):
        return self.conf['tmp_dir'], datetime.now().strftime('%Y_%m_%d_%H_%M_%S_') + self.name

    @staticmethod
    def _verify_verbatim_in_quotes(v):
        # remove all quote characters
        v = v\
            .replace('"', ' ')\
            .replace('\r', '')\
            .replace('\n', '')
        # put in quotes
        v = '"' + v + '"'
        return v

    def download_results(self, task):
        # download results
        tmp_dir = str(uuid.uuid4()) + '/'

        # creating temporary directories
        os.mkdir(self.conf['tmp_dir'] + tmp_dir)
        os.mkdir(self.conf['tmp_dir'] + tmp_dir + task['result']['files']['results'])
        os.mkdir(self.conf['tmp_dir'] + tmp_dir + task['result']['files']['failed'])

        # writing command files to download results from object store
        command_filename = self.conf['tmp_dir'] + 'swift_command_' + self.name + '.sh'
        f = open(command_filename, 'w')
        f.write(self.conf['swiftclient']['command']['header'] + '\n')
        f.write(self.conf['swiftclient']['command']['env_conf'] + '\n')
        f.write(
            self.conf['swiftclient']['command']['download']["results"]
            .replace('$ROOTDIRNAME$', self.conf['tmp_dir'] + tmp_dir)
            .replace('$SWIFTCLIENT$', self.conf['swiftclient']['client'])
            .replace('$CONTAINER$', task['result']['container'])
            .replace('$RESULTFILE$', task['result']['files']['results'])
            + '\n'
        )
        f.write(
            self.conf['swiftclient']['command']['download']["failed"]
            .replace('$ROOTDIRNAME$', self.conf['tmp_dir'] + tmp_dir)
            .replace('$SWIFTCLIENT$', self.conf['swiftclient']['client'])
            .replace('$CONTAINER$', task['result']['container'])
            .replace('$RESULTFILE$', task['result']['files']['failed'])
            + '\n'
        )
        f.close()
        if_downloaded = False
        _try = 0
        status = False

        while not if_downloaded:
            # do download
            subprocess.call(['sh', command_filename])
            # verify if downloaded
            if os.path.exists(self.conf['tmp_dir'] + tmp_dir + task['result']['files']["results"]) \
                    and os.path.exists(self.conf['tmp_dir'] + tmp_dir + task['result']['files']["failed"]):
                if_downloaded = True
                status = True

            if _try > 3:
                if_downloaded = True
                print '-- ERROR:', 'Could not download result file, max tries exceeded!'

        print '------------'
        print '-- DOWNLOADED AT', os.getcwd(), self.conf['tmp_dir'] + tmp_dir
        print '------------'

        return status, self.conf['tmp_dir'] + tmp_dir

    @staticmethod
    def _remove_tmp_download_files(tmp_dir):
        print '-- DELETING TMP DIR', tmp_dir
        shutil.rmtree(tmp_dir)
