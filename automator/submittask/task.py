# -*- coding: utf-8 -*-
from datetime import datetime
from pymongo import MongoClient
import json
from csv import reader
from pyspark import SparkContext


class SubmitTask(object):
    client = None
    _conf_map = {
        'NERTASK': 'ner',
    }
    if_completed = False
    job_id = None

    def __init__(self, name, conf):
        self.name = name
        self.conf = conf
        self.sc = SparkContext(appName="SubmitTask_" + name)
        # search query
        self.search_query = {
            'type': self._conf_map[self.name],
            'state': 'inactive',
        }

        # TODO temporary hadoop configurations
        # hadoop configurations
        h_cred = dict(
            name='spark',
            auth_url='https://identity.open.softlayer.com/v3',
            project_id='6c9da49a81da4859bf366da4d94603c5',
            user_id='2a13810f3d164885a7c33e5894bddd6b',
            password='RVz4/Ftgb!z1~U,?',
            region='dallas',
        )
        self._setup_hadoop_confs(h_cred)

    def _setup_db_connection(self):
        self.client = MongoClient(self.conf['db']['host'])

    def _close_db_connection(self):
        self.client = None

    def _setup_hadoop_confs(self, credentials):
        prefix = "fs.swift.service." + credentials['name']
        hconf = self.sc._jsc.hadoopConfiguration()
        hconf.set(prefix + ".auth.url", credentials['auth_url'] + '/auth/tokens')
        hconf.set(prefix + ".auth.endpoint.prefix", "endpoints")
        hconf.set(prefix + ".tenant", credentials['project_id'])
        hconf.set(prefix + ".username", credentials['user_id'])
        hconf.set(prefix + ".password", credentials['password'])
        hconf.setInt(prefix + ".http.port", 8080)
        hconf.set(prefix + ".region", credentials['region'])
        hconf.setBoolean(prefix + ".public", True)

    def __del__(self):
        if self.job_id:
            if self.if_completed:
                # set done if completed
                self._update_job_state('done')
            else:
                # set inactive if not completed
                self._update_job_state('inactive')

    def _update_job_state(self, state):
        self._setup_db_connection()

        self.client.bm.tasks.update(
            {'_id': self.job_id},
            {'$set': {
                'state': state
            }}
        )
        self._close_db_connection()

    def get_pending_task(self):
        self._setup_db_connection()
        job = self.client.bm.tasks.find_one(self.search_query)
        self._close_db_connection()

        if job:
            # set job state to active
            self.job_id = job['_id']
            self._update_job_state('active')

        return job

    def get_file_content(self, job):
        # fetch file from object storage
        print '-----------------------'
        print '-- Accessing Job_Filename', '\n'
        print '-----------------------'
        # tmp_dir = '/Users/arj/Downloads/'
        # text_file = self.sc.textFile(tmp_dir + job['filename'])  # dev

        text_file = self.sc.textFile('swift://notebooks.spark/' + job['filename'])  # prod

        print '-----------------------'
        print '-- Job File Count ', text_file.count(), '\n'
        print '-----------------------'

        return text_file


    @staticmethod
    def parse_content_verbatim(text_file):
        list_verbatims = text_file.flatMap(lambda d: json.loads(d)).map(
            lambda d: [t for t in reader([d.encode('utf-8')])][0])
        list_verbatims.count()

        return list_verbatims
