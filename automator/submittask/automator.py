from pymongo import MongoClient
import argparse
from __init__ import get_task


class Automator(object):
    conf = None
    client = None
    task = None

    def __init__(self):
        print '---------------------'
        print '---------------------'
        print '---------------------'
        print 'Init Automator'

        self._setup_args_parser()
        self.parse_args()

        # setup required variables
        self.setup_task()

    def _setup_args_parser(self):
        self.parser = argparse.ArgumentParser(description="Submit_Job Automator")
        self.parser.add_argument('name', type=str, help='Name of the Submit Job to run')
        self.parser.add_argument('db', type=str, help='mongo url for connection to the database')

    def parse_args(self):
        args = self.parser.parse_args()
        self.conf = dict(
            db=dict(
                host=args.db
            ),
            task_name=args.name,
        )

    def setup_task(self):
        self.task = get_task(self.conf['task_name'], {
            'db': {
                'host': self.conf['db']['host']
            }
        })

    def do_task(self):
        print '-----------------------'
        print '-----------------------'
        print '-----------------------'
        print '-- Doing Tasks'
        print self.task

        self.task.do_task()

if __name__ == '__main__':
    print '-----------------------'
    print '-----------------------'
    print '-----------------------'
    print '-----------------------'
    print '-- START'
    automator = Automator()
    # do task
    if automator.task:
        automator.do_task()
    print '-----------------------'
    print '-----------------------'
    print '-----------------------'
    print '-----------------------'
    print '-- END'
