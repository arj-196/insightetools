import uuid
from task import SubmitTask
from nertaggerwrapper import nertaggerwrapper


class NerTask(SubmitTask):
    def __init__(self, conf):
        super(NerTask, self).__init__("NERTASK", conf)
        self.tagger = nertaggerwrapper.NerTaggerWrapper()

    def do_task(self):
        job = self.get_pending_task()
        if job:
            print '-----------------------'
            print '-- ', self.name, 'doing task', '\n'
            print '-----------------------'
            # get file content
            content = self.get_file_content(job)

            # parse file content
            verbatims = self.parse_content_verbatim(content)
            print '-----------------------'
            print '-- ', self.name, 'parsed verbatims', '\n'
            print verbatims.take(5)
            print '-----------------------'

            # perform task on the verbatims RDD
            results = self.do_ner(verbatims)

            print '-----------------------'
            print '-- ', self.name, 'ner extracted', '\n'
            print results['ner_results'].take(5)
            print '-----------------------'

            print '-----------------------'
            print '-- ', self.name, 'failed verbatims', '\n'
            print results['failed_verbatims'].take(5)
            print '-----------------------'

            # save results to object store
            result_location, failed_location = self.save_results(results, job)
            print '-----------------------'
            print '-- ', self.name, 'Saved Results at', '\n'
            print result_location, failed_location
            print '-----------------------'

            # update job task to with results location
            if self.update_job_with_results((result_location, failed_location), job):
                # set that job has been completed
                self.if_completed = True

            print '-----------------------'
            print '-- ', self.name, 'Submit Task Completed', '\n'
            print '-----------------------'

    def save_results(self, results, job):
        # generate unique id
        unique_id = str(uuid.uuid4())
        results_filename = "ner_results_pickle_" + unique_id
        failed_filename = "ner_failed_pickle_" + unique_id

        # # dev
        # results['ner_results'].saveAsPickleFile("tmp_dir/" + results_filename)
        # results['failed_verbatims'].saveAsPickleFile("tmp_dir/" + failed_filename)

        # # prod
        # save file to outwput container
        results['ner_results'].saveAsPickleFile("swift://output.spark/" + results_filename)
        results['failed_verbatims'].saveAsPickleFile("swift://output.spark/" + failed_filename)

        return results_filename, failed_filename

    def update_job_with_results(self, locations, job):
        self._setup_db_connection()
        self.client.bm.tasks.update(
            {'_id': job['_id']},
            {'$set': {
                'result': {
                    'container': 'output',
                    'files': {
                        'results': locations[0],
                        'failed': locations[1]
                    }
                },
                'state': 'done',
            }}
        )
        self._close_db_connection()

        return True

    def do_ner(self, verbatims):
        tagger = self.tagger

        def get_ner(d):
            """
            function to get ner for verbatim
            :param d:
            :return:
            """
            if len(d) > 5:
                if d[5] == 'verbatim':
                    try:
                        return True, d[5], d[1], tagger.tag(d[3].split(' ')), d[2]
                    except:
                        return False, d[5], d[1], None
            return False, d

        def clean_ner(d):
            """
            function to clean ner result
            :param d:
            :return:
            """
            if len(d) > 2:
                if d[0]:
                    cleaned_ner = []
                    for t in d[3]:
                        if t[1] != 'O':
                            cleaned_ner.append(t)
                    return d[0], d[1], d[2], cleaned_ner, d[4]
                else:
                    return False, d
            else:
                return False, d

        # get ner results
        ner_result = verbatims.map(get_ner)
        # cleaning ner results
        cleaned_ner_results = ner_result.map(clean_ner)

        # finally to get verbatims for which ner caused an exception
        verbatim_keys = verbatims.map(lambda d: (d[1], True) if d and len(d) > 1 else (None, False))
        ner_keys = ner_result.map(lambda d: (d[2], d[0]) if d and len(d) > 2 else (None, False))
        # joining verbatim and ner keys
        verbatims_ner_status = verbatim_keys.join(ner_keys)

        failed_verbatims = verbatims_ner_status.filter(
            lambda d: d[1][1] == False if d and len(d) > 1 and len(d[1]) > 1 else None)

        return dict(
            ner_results=cleaned_ner_results,
            failed_verbatims=failed_verbatims,
        )
