#!/usr/bin/env bash

start_time="$(date '+%Y-%m-%d %H:%M:%S')"

# import environment variables
source ${1}env_vars.sh

# performing common submit task protocols
source ${1}launch_submit_task.sh

if [ ${IE_IF_CONTINUE} -eq 1 ]; then

    echo '---------------------------'
    echo 'Launch BM_SUBMIT_JOB_NER_TASK'
    echo '---------------------------'
    echo 'INFO: executing command:'
    # printing command
    echo ${IE_SPARK_SUBMIT_DIR}/spark-submit.sh \
        --vcap ${IE_VCAP_DIR}/vcap-${min_proc_index}.json \
        --deploy-mode cluster \
        --master https://169.54.219.20 \
        ${IE_BUNDLE_DIR}/bundled_submittask.py ner mongodb://151.80.41.13:27019

    # executing command
    sh ${IE_SPARK_SUBMIT_DIR}/spark-submit.sh \
        --vcap ${IE_VCAP_DIR}/vcap-${min_proc_index}.json \
        --deploy-mode cluster \
        --master https://169.54.219.20 \
        ${IE_BUNDLE_DIR}/bundled_submittask.py ner mongodb://151.80.41.13:27019

else
    echo "--------------------------"
    echo "ERROR BM_SUBMIT_JOB_NER_TASK"
    echo "Too many Spark-Submit Processes running."
    echo "--------------------------"
fi

# end
echo '---------------------------'
end_time="$(date '+%Y-%m-%d %H:%M:%S')"
echo "Start Time  :   ${start_time}"
echo "End Time    :   ${end_time}"
echo '----------- END -----------'
