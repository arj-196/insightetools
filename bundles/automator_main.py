import sys, os, json

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../unitools/')
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../')

from automator.automatetask import automator

#
# configurations
json.dump({
    "tmp_dir": "./tmp_dir/",
    "object_storage": {
        "auth_url": "https://identity.open.softlayer.com/v3",
        "password": "RVz4/Ftgb!z1~U,?",
        "project_id": "6c9da49a81da4859bf366da4d94603c5",
        "user_id": "2a13810f3d164885a7c33e5894bddd6b",
        "region_name": "dallas",
        "container_upload": "notebooks"
    },
    "registered_tasks": [
        {
            "type": "ner",
            "options": {}
        }
    ],
    "db": {
        # "host": "localhost:27017"
        "host": "mongodb://151.80.41.13:27019"
    },
    "swiftclient": {
        "client": "/Users/arj/anaconda/bin/swift",
        "command": {
            "header": "#!/usr/bin/env bash",
            "env_conf": "export OS_USER_ID=2a13810f3d164885a7c33e5894bddd6b;"
                        "export OS_PASSWORD=RVz4/Ftgb!z1~U,?; "
                        "export OS_PROJECT_ID=6c9da49a81da4859bf366da4d94603c5; "
                        "export OS_AUTH_URL=https://identity.open.softlayer.com/v3; "
                        "export OS_REGION_NAME=dallas; export OS_IDENTITY_API_VERSION=3; "
                        "export OS_AUTH_VERSION=3",
            "download": {
                "results": "cd $ROOTDIRNAME$; $SWIFTCLIENT$ download $CONTAINER$ --prefix $RESULTFILE$",
                "failed": "$SWIFTCLIENT$ download $CONTAINER$ --prefix $RESULTFILE$"
            }
        }
    }
}, open('./bm_confs_automate.json', 'w'), encoding='utf-8')


def main():
    if sys.argv[2:]:
        mode = sys.argv[1]
        c_file = sys.argv[2]
        if os.path.exists(c_file):
            if mode in ['pre', 'post']:
                # get confs
                conf = json.load(open(c_file, 'r'), encoding='utf-8')
                manager = automator.Automator(conf=conf)
                manager.automate(mode=mode)
            else:
                raise Exception("Unknown Mode Supplied")
            print '----- END'

        else:
            raise Exception("Configuration file not found" + c_file)
    else:
        raise Exception("Configuration file not supplied")


if __name__ == '__main__':
    main()


#
# python automator_main.py bm_automate_confs.json
