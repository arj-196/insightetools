#!/usr/bin/env bash

start_time="$(date '+%Y-%m-%d %H:%M:%S')"

# import environment variables
source ${1}env_vars.sh

echo '---------------------------'
echo '---------------------------'
echo 'Launch Postliminaries'

${IE_SPARK_SUBMIT_HOME} \
--master ${IE_MASTER_URL} \
${1}automator_main.py post bm_confs_automate.json

# end
echo '---------------------------'
echo '---------------------------'
echo '----------- END -----------'

end_time="$(date '+%Y-%m-%d %H:%M:%S')"
echo "Start Time  :   ${start_time}"
echo "End Time    :   ${end_time}"
