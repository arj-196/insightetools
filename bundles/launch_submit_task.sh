#!/usr/bin/env bash

echo '---------------------------'
echo '---------------------------'
echo 'Launch BM_SUBMIT_SCHEDULER'

export PYSPARK_PYTHON=/Users/arj/anaconda/bin/python

target_script_name=spark-submit
#target_script_name=spark_submit  # test : target launch_2_spark_submit instead

running_jobs="$(ps -ax | grep ${target_script_name} | wc -l | awk '{print $1}')"

if [ ${running_jobs} -lt ${max_parallel_job_count} ]; then
    IE_IF_CONTINUE=1
else
    IE_IF_CONTINUE=0
fi

echo '---------------------------'
echo "INFO: currently running jobs: ${running_jobs}"

# choosing vcap file
nb_vcaps=(1 2 3 4 5 6) # exists 6 spark services
procs_per_vcaps=()
for i in  ${nb_vcaps[*]}
do
#    procs_per_vcaps[${i}]="$(jot -r 1  5 200)"  # test : generate random integer
    procs_per_vcaps[${i}]="$(ps -ax | grep ${target_script_name} | grep vcap-${i}.json | wc -l | awk '{print $1}')"
done
echo '---------------------------'
echo "INFO: procs per vcaps [${procs_per_vcaps[*]}]"

echo '---------------------------'
echo 'INFO: getting min proc for vcap'

min_proc=${procs_per_vcaps[1]}
#min_proc=2
min_proc_index=1
# looping through procs_per_vcaps to find minimum value
for i in ${nb_vcaps[*]}
do
    if [ "${procs_per_vcaps[${i}]}" -lt  "${min_proc}" ];
    then
        min_proc=${procs_per_vcaps[${i}]}
        min_proc_index=${i}
    fi
done
echo "INFO: choosen vcap: ${min_proc_index}"
echo "INFO: nb current processes : ${min_proc}"
