# -*- coding: utf-8 -*-
import sys, traceback
import os

# sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../unitext/')  # local
# sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../unitools/')  # local
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../')  # db server

if len(sys.argv) < 4:
    raise Exception("Not enough parameters")
os.environ['PATH_RESOURCES'] = os.path.dirname(os.path.realpath(__file__)) + '/' + sys.argv[1]

from nertaggerwrapper.nertaggerwrapper import NerTaggerWrapper
from unitext.insight import resources as rs
from unitext.insight.conceptapi import ConceptNetWebAPI

import hashlib as hl
from datetime import datetime
from pymongo import MongoClient
startTime = datetime.now()

client = MongoClient(rs.HOST)


def _get_verbatims(topic, mode):
    verbatims = [d for d in client.indexes.verbatims.find(
        {'$and': [
            {'topic': topic},
            {'conf.' + mode: {'$ne': True}}
        ]},
        {'verbatim': 1, 'conf': 1}
    )]
    if len(verbatims) == 0:
        raise Exception("No Verbatims Found!")

    print 'nb of verbatims', len(verbatims)
    return verbatims


def _get_keywords(verbatimids):
    items = [d for d in client.indexes.verbatimkeywords.find({'_id': {'$in': verbatimids}}, {'keywords': 1})]
    keywords = {}
    for item in items:
        keywords[item['_id']] = item['keywords']
    return keywords


def _get_lang(topic):
    d = client.indexes.settings.find_one({'topic': topic})
    if 'lang' in d:
        return d['lang']
    else:
        raise Exception("No language found for topic!")


def _chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]


def extract_ner(topic):
    chunks = _chunks(_get_verbatims(topic, 'ner'), 10)
    lang = _get_lang(topic)
    tagger = NerTaggerWrapper()

    def clean_ner(ners):
        """
        function to clean ner result
        :param d:
        :return:
        """
        cleaned_ner = []
        for t in ners:
            if t[1] != 'O':
                cleaned_ner.append(t)
        return cleaned_ner

    def format_ner(ner_list):
        """
        Format for saving in database
        :param ner_list:
        :return:
        """
        ners = {}
        for n in ner_list:
            if n[1] not in ners:
                ners[n[1]] = {}

            en = n[0].replace('.', '')
            if en not in ners[n[1]]:
                ners[n[1]][en] = 1
            else:
                ners[n[1]][en] += 1
        return ners

    _iter = 0
    for chunk in chunks:
        items = []
        for verbatim in chunk:
            verbatim['ners'] = format_ner(clean_ner(
                tagger.tag(verbatim['verbatim'].split(' '))
            ))
            items.append(verbatim)
            _iter += 1

            if _iter % 10 == 0:
                print 'treated', _iter, 'verbatims', datetime.now() - startTime

        # saving in database
        for item in items:
            # update conf
            if 'conf' not in item:
                updateQuery = {'conf': {'ner': True}}
            else:
                updateQuery = {'conf.ner': True}
            #
            client.indexes.verbatims.update(
                {'_id': item['_id']},
                {'$set': updateQuery}
            )

            # update ner
            client.indexes.verbatimners.update(
                {'_id': item['_id']},
                {'$set': {
                    'entities': item['ners'],
                    'topic': topic,
                    'enabled': True,
                }},
                upsert=True
            )


def remove_non_ascii(text):
    return ''.join([i if ord(i) < 128 else ' ' for i in text])


def extract_concept(topic):
    verbatims = _get_verbatims(topic, 'concept')
    keywords = _get_keywords([d['_id'] for d in verbatims])
    lang = _get_lang(topic)
    conceptapi = ConceptNetWebAPI()
    _iter = 0
    for chunk in _chunks(verbatims, 20):
        items = []
        for verbatim in chunk:
            _iter += 1
            if verbatim['_id'] in keywords:
                concepts = []
                for keyword in keywords[verbatim['_id']][:2]:
                    concepts.append(conceptapi.get_concepts(keyword, lang))
                verbatim['concepts'] = concepts
                items.append(verbatim)

            if _iter % 10 == 0:
                print 'treated', _iter, 'verbatims', datetime.now() - startTime

        # saving to db
        for item in items:
            relations = []
            weights = []
            concepts = []
            id_concepts = []

            for o in item['concepts']:
                for c_key in o:
                    c_obj = o[c_key]
                    try:
                        id_concept = hl.md5(remove_non_ascii(c_key)).hexdigest()
                        id_concepts.append(id_concept)
                        weights.append(c_obj['weight'])
                        relations.append(c_obj['rel'])
                        concepts.append(c_obj['end'])
                    except:
                        e = sys.exc_info()[0]
                        print ("ERROR %s", e)
                        traceback.print_exc(file=sys.stdout)

            if 'conf' not in item:
                updateQuery = {'conf': {'concept': True}}
            else:
                updateQuery = {'conf.concept': True}
            #
            client.indexes.verbatims.update(
                {'_id': item['_id']},
                {'$set': updateQuery}
            )

            # update ner
            client.indexes.verbatimners.update(
                {'_id': item['_id']},
                {'$set': {
                    'topic': topic,
                    'enabled': True,
                    '_id_concepts': id_concepts,
                    'weights': weights,
                    'relations': relations,
                    'concepts': concepts,
                }},
                upsert=True
            )


if __name__ == '__main__':
    argv = sys.argv
    if argv[2] == 'ner':
        print 'Mode: NER'
        extract_ner(argv[3])
    elif argv[2] == 'concept':
        print 'Mode: Concept'
        extract_concept(argv[3])


# python partial_extract.py local_resources.py ner topic_name
