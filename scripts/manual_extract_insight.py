import sys, traceback, os

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../')
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../')
root = os.getcwd()
os.environ['PATH_RESOURCES'] = root + '/local_resources_preprod.py'

import codecs
from pymongo import MongoClient
from datetime import datetime
startTime = datetime.now()

from unitools.file import reader
from unitext.insight import resources as rs
from unitext.insight import insightextractor
from unitext.insight.indexer import WordCloudIndexer
import insight_extractor_manager as automated_extractor


def extract(contents, topic, lang, project, company, user):
    extractor = insightextractor.BaseInsightExtactor(lang=lang)
    _iter = 0

    print '----------------'
    print '----------------'
    print '-- START INSIGHT EXTRACTION '

    for item in contents:
        verbatim = item['verbatim']
        del item['verbatim']
        prefecteddata = {'additional': item}
	try:
        	extractor.extract(verbatim, topic,project, user, company,prefetcheddata=prefecteddata,ifsave=True,ifconcept=False, ifkeyword=True,ifpos=True, ifsentiment=True, ifterm=True,
            ifemotion=True, ifNER=False,
            ifHashTags=True, ifMentions=True,
            ifUrl=True,
        )
	except:
		pass

        _iter += 1
        if _iter % 5000 == 0:
            print "treated", _iter, "verbatims", datetime.now() - startTime

    print '----------------'
    print '----------------'
    print '-- START WORDCLOUD INDEXING'

    # start wordcloud indexing
    wcindexer = WordCloudIndexer()
    wcindexer.index(topic)

    print '----------------'
    print '----------------'
    print '-- START NER EXTRACTION'
    client = MongoClient(rs.HOST)
    automated_extractor.execute_ner_extractor(client, topic)


    print '----------------'
    print '----------------'
    print '----------------'
    print '-- SUCCESS EXTRACTION COMPLETED'


def verify_file_format(filepath, topic, lang, project, company, user):
    contents = reader.ColumnFileReader(lang, codecs.open(filepath, 'r', encoding='utf-8')).read(seperator=',')

    print '-- verify verbatim file format'
    print '-- verbatim count : ', len(contents)
    if len(contents) > 3:
        print '-- 1'
        print contents[0]
        print '-- 2'
        print contents[1]
        print '-- 3'
        print contents[2]
        print '-- '
        confirm = raw_input("confirm proceed extraction y/N : ")
        if confirm != 'y':
            print 'Abortting Extraction'
        else:
            extract(contents, topic, lang, project, company, user)
    else:
        print '-- ERR : There are not even 3 verbatims in the file. Is that Normal ?'


def main():
    if len(sys.argv) < 7:
        print 'ERROR NOT ALL PARAMS DEFINED'
        print 'RUN MUST BE IN THE FORMAT'
        print 'python manual_extract_insight.py filepath topic_name lang project_name company_name user_name'
    else:
        print 'Manual Extraction'

        filepath = sys.argv[1]
        topic = sys.argv[2]
        lang = sys.argv[3]
        project = sys.argv[4]
        company = sys.argv[5]
        user = sys.argv[6]

        print '--- verify variables'
        print 'filepath', ':', filepath
        print 'topic', ':', topic
        print 'lang', ':', lang
        print 'project', ':', project
        print 'company', ':', company
        print 'user ', ':', user
        print '--- '

        confirm = raw_input("comfirm if proceed y/N : ")
        if confirm != 'y':
            print 'Abortting Extraction'
        else:
            verify_file_format(filepath, topic, lang, project, company, user)


if __name__ == '__main__':
    main()

#
# python manual_extract_insight.py filepath topic_name lang project_name company_name user_name
