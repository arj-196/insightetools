# -*- coding: utf-8 -*-
import sys, traceback
import os

sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../unitext/')
sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../unitools/')
# sys.path.append(os.path.dirname(os.path.realpath(__file__)) + '/../../')

os.environ['PATH_RESOURCES'] = os.path.dirname(os.path.realpath(__file__)) + '/local_resources.py'

from datetime import datetime
from pymongo import MongoClient
from unitext4.insight import resources as rs
from unitools2.mailer import resources as mrs
from unitools2.mailer import mailer

startTime = datetime.now()


def get_next_repair_time_frame():
    minute = datetime.now().minute
    hour = datetime.now().hour

    day = datetime.now().day
    month = datetime.now().month
    year = datetime.now().year

    sepD = '/'
    sepH = ':'
    t1 = str(day) + sepD + str(month) + sepD + str(year) + " "
    t2 = str(day) + sepD + str(month) + sepD + str(year) + " "

    if minute < 30:
        t1 += str(hour) + sepH + '30'
        t2 += str(hour) + sepH + '45'
    else:
        t1 += str(hour + 1) + sepH + '00'
        t2 += str(hour + 1) + sepH + '15'

    return t1, t2


def mail_all_users(subject):
    MAIL_CONTENT = {
        'dev_in_progress': {
            'subject': "InsightE Update",
            'body': """
                This mail is to inform you that we are going to have a scheduled update on the InsightE Platform. <br><br>
                InsightE will temporarily be unavailable for 15mins, from $DATE1$ till $DATE2$. <br><br>
                You will be alerted by mail once the update is completed. <br><br>
                We appologize for the inconvinience and thank you for your comprehension.
            """,
        },
        'back_on_track': {
            'subject': "InsightE Update Completed",
            'body': """
                This mail is to inform you that the scheduled update has been successfully completed. <br><br>
                You can now get back to work! Thank you again for your comprehension.
            """,
        },
    }
    db = MongoClient(rs.HOST).client
    ifsend = False
    mail_subject = None
    mail_body = None

    user_emails = [d['email'] for d in db.users.find(no_cursor_timeout=True) if 'email' in d]
    user_emails = list(set(user_emails))  # unique addresses
    print 'Sending Mail to: ---'
    print user_emails

    if subject == 'dev_in_progress':
        ifsend = True

        # mail subject
        mail_subject = MAIL_CONTENT[subject]['subject']
        # mail body
        t1, t2 = get_next_repair_time_frame()
        mail_body = MAIL_CONTENT[subject]['body']\
            .replace('$DATE1$', t1)\
            .replace('$DATE2$', t2)
    elif subject == 'back_on_track':
        ifsend = True

        # mail subject
        mail_subject = MAIL_CONTENT[subject]['subject']
        # mail body
        mail_body = MAIL_CONTENT[subject]['body']


    if ifsend:
        Mailer = mailer.Mailer()
        for email in user_emails:
            Mailer.send(
                mrs.INSIGHTADDR,
                email,
                mail_subject,
                mail_body
            )


if __name__ == '__main__':

    if sys.argv[1:]:
        mail_all_users(sys.argv[1])


#
# python mail_all_users.py dev_in_progress
# python mail_all_users.py back_on_track
#
